package com.nulooper.preprocess;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @date 2014/03/24
 * 
 */
public class Similarity {
	public static Set<String> stopTags;
	static {
		stopTags = new HashSet<String>();
		stopTags.add("u");// 助词
		stopTags.add("o");// 拟声词
		stopTags.add("q");// 量词
		stopTags.add("y");// 语气词
		stopTags.add("e");// 叹词
		stopTags.add("w");// 标点符号
		stopTags.add("c");// 连词
		stopTags.add("p");// 介词
	}

	/**
	 * 该map的key集合是词表，values集合是包含对应单词的IDF值.
	 */
	public static Map<String, Double> vocabIDFMap = new HashMap<String, Double>();

	public static Map<String, Integer> vocabIndexMap = new HashMap<String, Integer>();

	public static List<String> vocabulary = null;

	/**
	 * 文档数量
	 */
	public static int docCount = 0;
	/**
	 * 每一篇文档对应的tf.
	 */
	public static List<Map<String, Double>> dataSetTFIDFMapList = new ArrayList<Map<String, Double>>(
			20000);

	/**
	 * 稀疏矩阵，三数组存储.
	 */
	public static List<List<Triple>> tripleMatrix = new ArrayList<List<Triple>>(
			20000);

	public static void process() {
		try {
			try (BufferedReader reader = Files.newBufferedReader(
					Paths.get("resources/data/199801"),
					Charset.forName("UTF-8"))) {
				String line = null;
				String[] items = null;// 每一行文档根据两个空格分割的数组
				int wordCount = 0;// 每一篇文档去除停用词之后的词数量
				String word = null;// 词
				String tag = null;// 词的词性标记
				String[] twoItems = null;// 根据/分割得到的仅含词和标记的两个元素数组
				int size;// 每一行文档根据两个空格分割的数组的大小
				Map<String, Double> tfMap = null;// 统计当前文档的词频

				Map<String, Double> tDocsMap = null;// 统计当前文档中出现的词，若出现，则为1.
				
				while ((line = reader.readLine()) != null) {
					if(!line.isEmpty()){
						
					}
					if (line.isEmpty()) {
						items = line.split("  ");// 根据两个空格进行文档字符串分割.
						++docCount;// 每读一行，就增1
						tfMap = new HashMap<String, Double>();// 当前文档的tf.
						tDocsMap = new HashMap<String, Double>();
						wordCount = 0;// 初始化当前文档词数量.

						size = items.length;
						for (int i = 1; i < size; ++i) {
							twoItems = items[i].split("/");
							word = twoItems[0];
							tag = twoItems[1].substring(0, 0);
							// step1: 如果当前词是停用词，则直接跳过当前循环.
							if (stopTags.contains(tag)) {
								continue;
							}
							// step2: 统计当前文档中有效词的数量.
							++wordCount;
							// step3-1: 统计当前文档中每一个单词出现的词频.
							addWordToTFMap(word, tfMap);
							// step4-1: 构建当前文档词表，和统计文档词表中每一个词是否出现.
							tDocsMap.put(word, 1.0);
						}
					}
					// step3-2: 归一化词频.
					normalizeTF(wordCount, tfMap);
					dataSetTFIDFMapList.add(tfMap);
					// step4-2: 将当前tDocsMap添加至文档集合.
					addDocMapToDataSetMap(tDocsMap, vocabIDFMap);
				}
				// 对文档数目docCount取对数.
				double logDocsN = Math.log(docCount);
				// step4-3: 计算词表中每一个单词的idf.
				computeIDF(logDocsN, vocabIDFMap);

				vocabulary = new ArrayList<String>(vocabIDFMap.keySet());

				// step 4-4: 计算tfidf值.
				computeTFIDF(vocabIDFMap, dataSetTFIDFMapList);

				// step 4-5: 规范词表顺序
				createVocabIndexMap(vocabulary);

				// step 4-6:
				createTripleMatrix(dataSetTFIDFMapList, vocabIndexMap);

				// step5: 计算内积，即文档间的相似度,必须在3秒以内.
				long start = System.currentTimeMillis();
				computeSimilarity(tripleMatrix);
				long end = System.currentTimeMillis();
				System.out.println((end - start) * 1.0 / 1000);
				// step6: 将结果以CSV文件格式写入磁盘.
				// writeSimMatrixToFile(similarityMatrix);

				reader.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * step3-1: 统计当前文档中每一个单词出现的词频.
	 * 
	 * @param word
	 * @param tfMap
	 */
	public static void addWordToTFMap(String word, Map<String, Double> tfMap) {
		if (tfMap.containsKey(word)) {
			tfMap.put(word, tfMap.get(word) + 1.0);
		} else {
			tfMap.put(word, 1.0);
		}
	}

	/**
	 * step3-2: 归一化词频.
	 * 
	 * @param wordCount
	 * @param tfMap
	 */
	public static void normalizeTF(int wordCount, Map<String, Double> tfMap) {
		for (String key : tfMap.keySet()) {
			tfMap.put(key, tfMap.get(key) / wordCount);
		}
	}

	/**
	 * step4-2
	 * 
	 * @param tDocsMap
	 * @param vocabFreqMap
	 */
	public static void addDocMapToDataSetMap(Map<String, Double> tDocsMap,
			Map<String, Double> vocabFreqMap) {
		for (String key : tDocsMap.keySet()) {
			if (vocabFreqMap.containsKey(key)) {
				vocabFreqMap.put(key, vocabFreqMap.get(key) + 1.0);
			} else {
				vocabFreqMap.put(key, 1.0);
			}
		}
	}

	/**
	 * step4-3 计算词表的IDF值.
	 * 
	 * @param logN
	 * @param vocabFreqMap
	 */
	public static void computeIDF(double logDocsN,
			Map<String, Double> vocabFreqMap) {
		for (String key : vocabFreqMap.keySet()) {
			vocabFreqMap.put(key,
					logDocsN - Math.log(vocabFreqMap.get(key).doubleValue()));
		}
	}

	/**
	 * step4-4
	 * 
	 * @param vocabMap
	 * @param dataSetTFMap
	 */
	public static void computeTFIDF(Map<String, Double> vocabMap,
			List<Map<String, Double>> dataSetTFMapList) {
		for (Map<String, Double> map : dataSetTFMapList) {
			for (String word : map.keySet()) {
				map.put(word, map.get(word) * vocabMap.get(word));
			}
		}
	}

	/**
	 * step4-5:
	 * 
	 * @param vocabulary
	 */
	public static void createVocabIndexMap(List<String> vocabulary) {
		int size = vocabulary.size();
		for (int i = 0; i < size; ++i) {
			vocabIndexMap.put(vocabulary.get(i), i);
		}
	}

	/**
	 * step4-6:
	 * 
	 * @param dataSetTFIDFMapList
	 * @param vocabIDFMap
	 */
	public static void createTripleMatrix(
			List<Map<String, Double>> dataSetTFIDFMapList,
			Map<String, Integer> vocabIndexMap) {
		List<String> words = null;
		List<Triple> doc = null;
		int size = dataSetTFIDFMapList.size();
		Map<String, Double> tfidfMap = null;

		int row;

		for (int column = 0; column < size; ++column) {
			doc = new ArrayList<Triple>();
			tfidfMap = dataSetTFIDFMapList.get(column);
			words = new ArrayList<String>(tfidfMap.keySet());
			for (String word : words) {
				row = vocabIndexMap.get(word).intValue();
				doc.add(new Triple(row, column, tfidfMap.get(word)));
			}
			// 对doc元素根据行号，进行排序.
			Collections.sort(doc, new Comparator<Triple>() {

				@Override
				public int compare(Triple o1, Triple o2) {
					return o1.row - o2.row;
				}

			});
			tripleMatrix.add(doc);
		}

	}

	/**
	 * 计算内积，即相似度.
	 * 
	 * @param tripleMatrix
	 */
	public static void computeSimilarity(List<List<Triple>> tripleMatrix) {
		List<Triple> doc1, doc2;
		Triple[] doc1Array, doc2Array, min;
		int doc1Size, doc2Size;
		Triple left, right;
		Comparator<Triple> comparator = new Comparator<Triple>() {

			@Override
			public int compare(Triple o1, Triple o2) {
				// TODO Auto-generated method stub
				return o1.row - o2.row;
			}

		};
		for (int i = 0; i < docCount; ++i) {
			doc1 = tripleMatrix.get(i);
			doc1Size = doc1.size();
			doc1Array = new Triple[doc1Size];
			doc1.toArray(doc1Array);
			for (int j = i + 1; j < docCount; ++j) {
				doc2 = tripleMatrix.get(j);
				doc2Size = doc2.size();
				doc2Array = new Triple[doc2Size];
				doc2.toArray(doc2Array);
				// 计算内积.
				double innerProduct = 0.0;
				int index;
				for (Triple triple : doc1) {
					index = Arrays.binarySearch(doc2Array, triple, comparator);
					if (index >= 0) {
						innerProduct += triple.tfidf * doc2Array[index].tfidf;
					}
				}
				// System.out.println(innerProduct);
			}
			System.out.println("处理完成第" + i + "个文档");
		}
	}

	/**
	 * step6: 将结果相似度值矩阵以CSV格式写入文件.
	 * 
	 * @param similarityMatrix
	 */
	public static void writeSimMatrixToFile(
			Map<Integer, TreeMap<InnerProduct, Integer>> similarityMatrix) {
		// wait to implement.
	}

	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		process();
		long end = System.currentTimeMillis();
		System.out.println((end - start) * 1.0 / 1000);
	}

}
