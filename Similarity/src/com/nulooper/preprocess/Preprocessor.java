package com.nulooper.preprocess;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.nulooper.utils.IOHelper;

/**
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @date 2014/03/24
 * 
 */
public class Preprocessor {
	public static Set<String> stopTags;
	static {
		stopTags = new HashSet<String>();
		stopTags.add("u");// 助词
		stopTags.add("o");// 拟声词
		stopTags.add("q");// 量词
		stopTags.add("y");// 语气词
		stopTags.add("e");// 叹词
		stopTags.add("w");// 标点符号
		stopTags.add("c");// 连词
		stopTags.add("p");// 介词
	}

	/**
	 * 该map的key集合是词表，values集合是包含对应单词的IDF值.
	 */
	public static Map<String, Double> vocabIDFMap = new HashMap<String, Double>();

	public static Map<String, Integer> vocabIndexMap = new HashMap<String, Integer>();

	public static List<String> vocabulary = null;

	/**
	 * 文档数量
	 */
	public static int docCount = 0;
	/**
	 * 每一篇文档对应的tf.
	 */
	public static List<Map<String, Double>> dataSetTFIDFMapList = new ArrayList<Map<String, Double>>(
			3443);

	public static void process() {
		try {
			List<String> lines = IOHelper.readLines("resources/data/199801");
			docCount = lines.size();
			String[] items = null;// 每一行文档根据两个空格分割的数组
			int wordCount = 0;// 每一篇文档去除停用词之后的词数量
			String word = null;// 词
			String tag = null;// 词的词性标记
			String[] twoItems = null;// 根据/分割得到的仅含词和标记的两个元素数组
			int size;// 每一行文档根据两个空格分割的数组的大小
			Map<String, Double> tfMap = null;// 统计当前文档的词频

			Map<String, Double> tDocsMap = null;// 统计当前文档中出现的词，若出现，则为1.

			for (String line : lines) {

				items = line.split("  ");// 根据两个空格进行文档字符串分割.
				tfMap = new HashMap<String, Double>();// 当前文档的tf.
				tDocsMap = new HashMap<String, Double>();
				wordCount = 0;// 初始化当前文档词数量.

				size = items.length;
				for (int i = 1; i < size; ++i) {
					twoItems = items[i].split("/");
					word = twoItems[0].trim();
					tag = twoItems[1].trim().substring(0, 0);
					// step1: 如果当前词是停用词，则直接跳过当前循环.
					if (stopTags.contains(tag)) {
						continue;
					}
					// step2: 统计当前文档中有效词的数量.
					++wordCount;
					// step3-1: 统计当前文档中每一个单词出现的词频.
					addWordToTFMap(word, tfMap);
					// step4-1: 构建当前文档词表，和统计文档词表中每一个词是否出现.
					tDocsMap.put(word, 1.0);
				}
				// step3-2: 归一化词频.
				normalizeTF(wordCount, tfMap);
				dataSetTFIDFMapList.add(tfMap);
				// step4-2: 将当前tDocsMap添加至文档集合.
				addDocMapToDataSetMap(tDocsMap, vocabIDFMap);
			}
			// 对文档数目docCount取对数.
			double logDocsN = Math.log(docCount);
			// step4-3: 计算词表中每一个单词的idf.
			computeIDF(logDocsN, vocabIDFMap);

			// step 4-4: 计算tfidf值.
			computeTFIDF(vocabIDFMap, dataSetTFIDFMapList);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * step3-1: 统计当前文档中每一个单词出现的词频.
	 * 
	 * @param word
	 * @param tfMap
	 */
	public static void addWordToTFMap(String word, Map<String, Double> tfMap) {
		if (tfMap.containsKey(word)) {
			tfMap.put(word, tfMap.get(word) + 1.0);
		} else {
			tfMap.put(word, 1.0);
		}
	}

	/**
	 * step3-2: 归一化词频.
	 * 
	 * @param wordCount
	 * @param tfMap
	 */
	public static void normalizeTF(int wordCount, Map<String, Double> tfMap) {
		for (String key : tfMap.keySet()) {
			tfMap.put(key, tfMap.get(key) / wordCount);
		}
	}

	/**
	 * step4-2
	 * 
	 * @param tDocsMap
	 * @param vocabFreqMap
	 */
	public static void addDocMapToDataSetMap(Map<String, Double> tDocsMap,
			Map<String, Double> vocabFreqMap) {
		for (String key : tDocsMap.keySet()) {
			if (vocabFreqMap.containsKey(key)) {
				vocabFreqMap.put(key, vocabFreqMap.get(key) + 1.0);
			} else {
				vocabFreqMap.put(key, 1.0);
			}
		}
	}

	/**
	 * step4-3 计算词表的IDF值.
	 * 
	 * @param logN
	 * @param vocabFreqMap
	 */
	public static void computeIDF(double logDocsN,
			Map<String, Double> vocabFreqMap) {
		for (String key : vocabFreqMap.keySet()) {
			vocabFreqMap.put(key,
					logDocsN - Math.log(vocabFreqMap.get(key).doubleValue()));
		}
	}

	/**
	 * step4-4
	 * 
	 * @param vocabMap
	 * @param dataSetTFMap
	 */
	public static void computeTFIDF(Map<String, Double> vocabMap,
			List<Map<String, Double>> dataSetTFMapList) {
		for (Map<String, Double> map : dataSetTFMapList) {
			for (String word : map.keySet()) {
				map.put(word, map.get(word) * vocabMap.get(word));
			}
		}
	}

	/**
	 * step4-5:
	 * 
	 * @param vocabulary
	 */
	public static void createVocabIndexMap(List<String> vocabulary) {
		int size = vocabulary.size();
		for (int i = 0; i < size; ++i) {
			vocabIndexMap.put(vocabulary.get(i), i);
		}
	}

	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		process();
		long end = System.currentTimeMillis();
		System.out.println((end - start) * 1.0 / 1000);
	}

}
