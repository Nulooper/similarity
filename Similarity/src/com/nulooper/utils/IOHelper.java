package com.nulooper.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @date 2014/03/25
 * 
 */
public class IOHelper {
	public static List<String> readLines(String path) throws IOException {
		BufferedReader reader = Files.newBufferedReader(Paths.get(path),
				Charset.forName("UTF-8"));
		String line;
		StringBuilder currentLine = new StringBuilder();
		List<String> newLines = new ArrayList<String>();
		while ((line = reader.readLine()) != null) {
			if (!line.isEmpty()) {
				currentLine.append(getContent(line));
			} else {
				if (!currentLine.toString().isEmpty())
					newLines.add(currentLine.toString());
				currentLine = new StringBuilder();
			}
		}
		newLines.add(currentLine.toString());
		return newLines;
	}

	public static String getContent(String line) {
		int index = line.indexOf("  ");
		return line.substring(index + 2);
	}
}
